/*
 * Copyright (C) 2022 UBports Foundation.
 * Author(s): Lorenzo Torracchi <lorenzotorracchi@mail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timelapseworker.h"

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/utils/logger.hpp>

#include <QDebug>
#include <QString>
#include <QDateTime>
#include <QDir>
#include <QFile>

using namespace cv;

TimeLapseWorker::TimeLapseWorker(QString sourceDir, QString targetDir) {
    this->sourceDir = sourceDir;
    this->targetDir = targetDir;
}

void TimeLapseWorker::run() {
    utils::logging::setLogLevel(utils::logging::LOG_LEVEL_VERBOSE);
    QDateTime currentDateTime = QDateTime::currentDateTime();
    QString fileName = this->targetDir + "/" + currentDateTime.toString("yyyy-MM-dd-hh_mm_ss") + ".mp4";
    qDebug() << "************** outfile: " << fileName;

    QDir dir(this->sourceDir);
    QFileInfoList frames = dir.entryInfoList(QStringList() << "*.jpg" << "*.jpeg" << "*.JPG" << "*.JPEG", QDir::Files);
    if (frames.isEmpty()) {
        qWarning() << "TimeLapse: no frames available";
        return;
    }

    qDebug() << "************** Started writing video... ";
    Mat image = imread(frames.at(0).absoluteFilePath().toStdString().c_str(), IMREAD_COLOR);
    if (image.empty()) {
        qWarning() << "TimeLapse: cannot read image" << frames.at(0).absoluteFilePath();
        for (int i = 0; i < frames.size(); i++) {
            QFile file(frames.at(i).absoluteFilePath());
            file.remove();
        }
        return;
    }
    VideoWriter writer;
    int codec = VideoWriter::fourcc('H', '2', '6', '4');
    double fps = 2.0;
    writer.open(fileName.toStdString(), codec, fps, image.size(), true);
    if (!writer.isOpened()) {
        qWarning() << "TimeLapse: could not open the output video file for write";
        dir.removeRecursively();
        return;
    }
    qDebug() << "************** files: " << frames.size();
    for (int i = 0; i < frames.size(); i++) {
        image = imread(frames.at(i).absoluteFilePath().toStdString().c_str(), IMREAD_COLOR);
        if (image.empty()) {
            qWarning() << "TimeLapse: skipped empty image: " << frames.at(i).absoluteFilePath();
            continue;
        }
        writer.write(image);
        QFile file(frames.at(i).absoluteFilePath());
        file.remove();
    }
    qDebug() << "************** Write completed!";

    Q_EMIT timeLapseGenerated(fileName);
}
