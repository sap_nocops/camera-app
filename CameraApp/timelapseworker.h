/*
 * Copyright (C) 2022 UBports Foundation.
 * Author(s): Lorenzo Torracchi <lorenzotorracchi@mail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TIMELAPSEWORKER_H
#define TIMELAPSEWORKER_H

#include <QObject>
#include <QString>
#include <QRunnable>

class TimeLapseWorker : public QObject, public QRunnable
{
    Q_OBJECT

public:
    TimeLapseWorker(QString sourceDir, QString targetDir);
    void run();
signals:
    void timeLapseGenerated(QString filePath);
private:
    QString sourceDir;
    QString targetDir;
};

#endif //TIMELAPSEWORKER_H
