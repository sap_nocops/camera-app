/*
 * Copyright (C) 2022 UBports Foundation.
 * Author(s): Lorenzo Torracchi <lorenzotorracchi@mail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "timelapsecontroller.h"
#include "timelapseworker.h"

#include <QObject>
#include <QString>
#include <QThreadPool>

TimeLapseController::TimeLapseController(QObject *parent) : QObject(parent) {
}

void TimeLapseController::makeTimeLapseVideo(QString sourceDir, QString targetDir) {
    TimeLapseWorker *worker = new TimeLapseWorker(sourceDir, targetDir);

    connect(worker, SIGNAL (timeLapseGenerated(QString)), this, SIGNAL (timeLapseGenerated(QString)));
    QThreadPool::globalInstance()->start(worker);
}
